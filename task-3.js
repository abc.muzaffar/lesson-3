// ------ task-1 ------

const res = require("express/lib/response");

function fizzBuzz(num) {
  if (typeof num != "number") return "Enter only number!";
  if (num % 3 == 0 && num % 5 == 0) return "FizzBuzz";
  if (num % 3 == 0) return "Fizz";
  if (num % 5 == 0) return "Buzz";
  else return "an indivisible number of three and five";
}
// console.log(fizzBuzz(15));

// ------ task-2 ------

function arrayFiltered(array) {
  let result = [];
  array
    .filter((el) => typeof el == "object")
    .flat()
    .map((el) => {
      if (!result.includes(el)) result.push(el);
    });

  return sorted(result)
}

function sorted(params){
  return params.sort((a, b) => a - b);
}

// console.log(arrayFiltered([[2], 23, "dance", true, [3, 5, 3], [65, 45]]))

