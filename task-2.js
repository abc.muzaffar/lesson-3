// ------ task-1 ------

const res = require("express/lib/response");

function numberCheck(num) {
  if (typeof num != "number") return "Enter only number!";
  if (num % 5 == 0 && num % 3 == 0) return true;
  else return false;
}
// const result = numberCheck(15)
// console.log(result);

// ------ task-2 ------

function isNumberOddOrEven(num) {
  if (typeof num != "number") return "Enter only number!";
  if (num % 2 == 0) return "Even number";
  else return "Odd number";
}
// const result = isNumberOddOrEven(2);
// console.log(result);

// ------ task-3 ------

function sortArrayNumber(array) {
  // let result = array.sort((a, b) => a - b);
  // return result;

  for (let i = 0; i < array.length; i++) {
    for (let j = i + 1; j < array.length; j++) {
      if (array[i] > array[j]) {
        // let a = array.splice(j,1)[0]
        // array.splice(i,0,a)
        let a = array[i];
        array[i] = array[j];
        array[j] = a;
      }
    }
  }
  return array;
}
// const sorted = sortArrayNumber([5,2,4,1,3,-1])
// console.log(sorted);


// ------ task-4 ------

function uniqueSetArray(params) {
  let result = [];
  for (let j of params) {
    Object.values(j)[0].map((el) => {
      if (!result.includes(el)) result.push(el);
    });
  }
  return result;
}

let array = [
  { test: ["a", "b", "c", "d"] },
  { test: ["a", "b", "c"] },
  { test: ["a", "d"] },
  { test: ["a", "b", "k", "e", "e"] },
];
// const output = uniqueSetArray(array);
// console.log(output);


// ------ task-5 ------

function objectsCompare(obj1,obj2){
  let a = Object.entries(obj1).toString()
  let b = Object.entries(obj2).toString()
  return (a == b);
}

// const cat = {name: 'Pussy', age: 2};
// const dog = {name: 'Rex', age: 3};

// console.log(objectsCompare(cat,dog));


